from aocd import get_data

data = get_data(day=6, year=2020).split('\n')

# part 1
groups = []
this_group = []

for line in data:
    if line == '':
        groups.append(list(set(this_group)))
        this_group = []

    this_group += list(line)

groups.append(list(set(this_group)))
this_group = []

print(sum(len(thing) for thing in groups))

# part 2
groups = []
this_group = []
num = 0

for line in data:
    if line == '':
        a = []
        for letter in this_group:
            if this_group.count(letter) == num:
                a.append(letter)

        groups.append(list(set(a)))
        this_group = []
        num = 0
        continue

    num += 1

    this_group += list(line)


a = []
for letter in this_group:
    if this_group.count(letter) == num:
        a.append(letter)

groups.append(list(set(a)))
this_group = []

print(sum(len(thing) for thing in groups))
