from aocd import get_data

data = get_data(day=5, year=2020).split('\n')

result = data

def decode(board):
    front = board[:-3]
    side = board[-3:]

    row = 0
    for i, thing in enumerate(front[::-1]):
        row += 2 ** i if thing == 'B' else 0

    col = 0
    for i, thing in enumerate(side[::-1]):
        col += 2 ** i if thing == 'R' else 0

    return (row, col)

# part 1
print(max(decode(thing)[0] * 8 + decode(thing)[1] for thing in data))

# part 2
passes = sorted(decode(thing)[0] * 8 + decode(thing)[1] for thing in data)
not_there = []

for thing in range(8 * 128):
    if thing not in passes:
        not_there.append(thing)

for thing in not_there:
    if thing + 1 in passes and thing - 1 in passes:
        print(thing)
