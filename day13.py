from aocd import get_data

data = get_data(day=13, year=2020).split('\n')

cur = int(data[0])
blah = data[1].split(',')

lowe = 92835798235798237345
low = 387295692385792
low_at = 0

for thing in blah:
    if thing == 'x': continue
    thing = int(thing)
    next_ = (cur // thing) * thing

    if next_ < cur:
        next_ += thing

    time_til = next_ - cur

    if time_til < low:
        low = time_til
        lowe = time_til * thing

print(lowe)

# part 2
from modint import chinese_remainder

ns = []
mods = []

for i, thing in enumerate(blah):
    if thing == 'x': continue
    thing = int(thing)

    ns.append(thing)
    mods.append((0 - i) % thing)

print(chinese_remainder(ns, mods))
