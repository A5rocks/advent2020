from aocd import get_data

card, door = get_data(day=25, year=2020).split('\n')
#card, door = "5764801", "17807724"
card, door = int(card), int(door)

def transform(n, subj_num):
    n *= subj_num
    n %= 20201227
    return n

current_card = 7
i = 0

while current_card != card:
    i += 1
    current_card = transform(current_card, 7)

old_door = door

for n in range(i):
    door = transform(door, old_door)

print(door)
