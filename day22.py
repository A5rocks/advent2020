from aocd import get_data
from collections import deque

p1, p2 = get_data(day=22, year=2020).split('\n\n')

# part 1
p1_deck = deque(list(map(int, p1.split('\n')[1:])))
p2_deck = deque(list(map(int, p2.split('\n')[1:])))

while len(p1_deck) > 0 and len(p2_deck) > 0:
    print(p1_deck, p2_deck)
    p1_card = p1_deck.popleft()
    p2_card = p2_deck.popleft()

    if p1_card > p2_card:
        p1_deck.append(p1_card)
        p1_deck.append(p2_card)
    elif p2_card > p1_card:
        p2_deck.append(p2_card)
        p2_deck.append(p1_card)

result = 0

if len(p1_deck) > len(p2_deck):
    for i, card in enumerate(p1_deck):
        result += (len(p1_deck) - i) * card
else:
    for i, card in enumerate(p2_deck):
        result += (len(p2_deck) - i) * card

print(result)

# part 2
p1_deck = deque(list(map(int, p1.split('\n')[1:])))
p2_deck = deque(list(map(int, p2.split('\n')[1:])))


def calc(cards):
    res0 = 0
    for i, card in enumerate(cards[0]):
        res0 += (len(cards[0]) - i) * card

    res1 = 0
    for i, card in enumerate(cards[1]):
        res1 += (len(cards[1]) - i) * card

    return (res0, res1)

def hash_it(cards):
    res0 = 0
    res1 = 0

    maximum = max(max(cards[0]), max(cards[1]))
    for i, card in enumerate(cards[0]):
        res0 += i * card * maximum

    for i, card in enumerate(cards[1]):
        res1 += i * card * maximum

    return res0, res1

def game(cards):
    memo = set()
    while cards[0] and cards[1]:
        winner, card0, card1 = f(cards, memo)

        if winner == 1:
            cards[0].append(card0)
            cards[0].append(card1)
        else:
            cards[1].append(card1)
            cards[1].append(card0)

    return cards

def f(cards, memo):
    if hash((tuple(cards[0]), tuple(cards[1]))) in memo:
        card0 = cards[0].popleft()
        card1 = cards[1].popleft()
        while cards[1]:
            cards[1].pop()
        # we assume the base games are not going to be recursive
        return (1, card0, card1)

    memo.add(hash((tuple(cards[0]), tuple(cards[1]))))
    card0 = cards[0].popleft()
    card1 = cards[1].popleft()

    if len(cards[0]) >= card0 and len(cards[1]) >= card1:
        done = game((deque(list(cards[0].copy())[:card0]), deque(list(cards[1].copy())[:card1])))
        return (1 if len(done[0]) > len(done[1]) else 2, card0, card1)
    else:
        if card0 > card1:
            return (1, card0, card1)
        else:
            return (2, card0, card1)



print(calc(game((p1_deck, p2_deck))))
