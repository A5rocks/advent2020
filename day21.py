from aocd import get_data
import collections

data = get_data(day=21, year=2020).split('\n')
# data = """mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
# trh fvjkl sbzzf mxmxvkd (contains dairy)
# sqjhc fvjkl (contains soy)
# sqjhc mxmxvkd sbzzf (contains fish)""".split('\n')

possible_contents = collections.defaultdict(lambda: [])
lines_map = collections.defaultdict(lambda: [])
associated = collections.defaultdict(lambda: [])
allergens_map = {}
loose_allergens_map = {}
allergens_lines = collections.defaultdict(lambda: [])
allergens = []
ingredients = []

for i, line in enumerate(data):
    if '(' in line:
        ingr, alle = line.split(' (')
    else:
        ingr, alle = line, ''
    alle = alle.strip(')')
    alle = ''.join(alle.split('contains '))
    ingr = ingr.split(' ')
    alle = alle.split(', ')

    for ingredient in ingr:
        ingredients.append(ingredient)
        possible_contents[ingredient] += alle
        lines_map[ingredient].append(i)

    for allergen in alle:
        allergens.append(allergen)
        allergens_lines[allergen].append(i)
        associated[allergen] += ingr

allergens = list(set(allergens))

for i in range(100):
    for allergen, lines in allergens_lines.items():
        if allergen in allergens_map: continue
        perhaps = list(set(associated[allergen]))
        actually = []
        for perhap in perhaps:
            if possible_contents[perhap].count(allergen) == len(lines):
                actually.append(perhap)

        actually = [thing for thing in actually if thing not in allergens_map.values()]

        assert len(actually) > 0
        if len(actually) == 1:
            allergens_map[allergen] = actually[0]
        else:
            loose_allergens_map[allergen] = actually

result = 0

for ingredient in list(set(ingredients)):
    if ingredient not in allergens_map.values() and ingredient not in [thing for value in loose_allergens_map.values() for thing in value]:
        result += len(lines_map[ingredient])

# part 1
print(result)

# part 2
print(','.join(v for k, v in sorted(allergens_map.items(), key = lambda n: n[0])))
