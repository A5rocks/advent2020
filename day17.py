from aocd import get_data

data = get_data(day=17, year=2020).split('\n')

# part 1
grid_ = []

start = []

for line in data:
    start.append([])
    for char in line:
        start[-1].append(1 if char == '#' else 0)

grid_.append(start)

# simulation logic
def simulate(grid__):
    grid = [thing.copy() for thing in grid__]

    result = [[] for _ in range(len(grid) + 2)]

    for z in range(len(result)):
        changes_from = grid[max(z-2,0):z+1]
        prev = grid[z-1] if z != 0 and z != len(result) - 1 else None
        for y in range(len(changes_from[0]) + 2):
            result[z].append([])
            for x in range(len(changes_from[0][0]) + 2):
                alive = -prev[y-1][x-1] if y != 0 and y != len(changes_from[0]) + 1 and x != 0 and x != len(changes_from[0][0]) + 1 and prev else 0
                current_state = prev[y-1][x-1] if y != 0 and y != len(changes_from[0]) + 1 and x != 0 and x != len(changes_from[0][0]) + 1 and prev else 0
                for layer in changes_from:
                    for i in (-1, 0, 1):
                        for j in (-1, 0, 1):
                            if y + i < 1:
                                continue
                            if x + j < 1:
                                continue
                            if y + i > len(changes_from[0]):
                                continue
                            if x + j > len(changes_from[0][0]):
                                continue

                            alive += layer[y+i-1][x+j-1]

                if current_state == 0:
                    if alive == 3:
                        result[z][y].append(1)
                    else:
                        result[z][y].append(0)
                else:
                    if alive in [2, 3]:
                        result[z][y].append(1)
                    else:
                        result[z][y].append(0)

    return result

result = 0
for layer in simulate(simulate(simulate(simulate(simulate(simulate(grid_)))))):
    for row in layer:
        for thing in row:
            if thing == 1:
                result += 1

print(result)

# part 2
grid_ = []

start = []

for line in data:
    start.append([])
    for char in line:
        start[-1].append(1 if char == '#' else 0)

grid_.append(start)

grid___ = []
grid___.append(grid_)

# simulation logic
def simulate(grid__):
    grid = [thing.copy() for thing in grid__]

    result = [[] for _ in range(len(grid) + 2)]

    for z in range(len(result)):
        changes_from = grid[max(z-2,0):z+1]
        prev = grid[z-1] if z != 0 and z != len(result) - 1 else None
        for y in range(len(changes_from[0]) + 2):
            result[z].append([])
            for x in range(len(changes_from[0][0]) + 2):
                result[z][y].append([])
                for w in range(len(changes_from[0][0][0]) + 2):
                    alive = -prev[y-1][x-1][w-1] if y != 0 and y != len(changes_from[0]) + 1 and x != 0 and x != len(changes_from[0][0]) + 1 and w != 0 and w != len(changes_from[0][0][0]) + 1 and prev else 0
                    current_state = prev[y-1][x-1][w-1] if y != 0 and y != len(changes_from[0]) + 1 and x != 0 and x != len(changes_from[0][0]) + 1 and w != 0 and w != len(changes_from[0][0][0]) + 1 and prev else 0
                    for layer in changes_from:
                        for i in (-1, 0, 1):
                            for j in (-1, 0, 1):
                                for k in (-1, 0, 1):
                                    if y + i < 1:
                                        continue
                                    if x + j < 1:
                                        continue
                                    if y + i > len(changes_from[0]):
                                        continue
                                    if x + j > len(changes_from[0][0][0]):
                                        continue
                                    if w + k < 1:
                                        continue
                                    if w + k > len(changes_from[0][0][0]):
                                        continue

                                    alive += layer[y+i-1][x+j-1][w+k-1]

                    if current_state == 0:
                        if alive == 3:
                            result[z][y][x].append(1)
                        else:
                            result[z][y][x].append(0)
                    else:
                        if alive in [2, 3]:
                            result[z][y][x].append(1)
                        else:
                            result[z][y][x].append(0)

    return result

result = 0
for layer in simulate(simulate(simulate(simulate(simulate(simulate(grid___)))))):
    for row in layer:
        for thing in row:
            for n in thing:
                if n == 1:
                    result += 1

print(result)
