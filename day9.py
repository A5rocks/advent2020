from aocd import get_data

data = list(map(int, get_data(day=9, year=2020).split('\n')))

prev = []

# part 1
for thing in data:
    if len(prev) < 25:
        prev.append(thing)

    else:
        done = False
        for a in prev:
            if done:
                break
            for b in prev:
                if a + b == thing and a != b:
                    done = True
                    break
        else:
            print(thing)
            break

        prev.append(thing)

# part 2
invalid = 88311122
for i, thing in enumerate(data):
    summ = thing
    n = 1
    while summ < invalid:
        summ += data[i + n]
        n += 1

    if n == 2:
        continue

    if summ == invalid:
        print(min(data[i:i+n]) + max(data[i:n+i]))
