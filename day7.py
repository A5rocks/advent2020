from aocd import get_data

data = get_data(day=7, year=2020).split('\n')

can_contain = {}

for line in data:
    color, contains = line.split(' bags contain ', 1)
    can_contain[color] = contains

# part 1
last_res = []
result = ['shiny gold']
while list(set(last_res)) != list(set(result)):
    last_res = result.copy()
    for color in result:
        for k, v in can_contain.items():
            if color in v:
                result.append(k)


print(len(list(set(result))) - 1)

# part 2
def remove(s, t):
    return ''.join(s.split(t))

def parse(s):
    if 'no other bags.' == s: return []
    return [bag.split(' ', 1) for bag in remove(remove(s.strip('.'), ' bags'), ' bag').split(', ')]

def calculate(s):
    bags = parse(can_contain[s])
    res = 1
    for n, typ in bags:
        res += int(n) * calculate(typ)
    return res


print(calculate('shiny gold') - 1)