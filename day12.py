from aocd import get_data
import math

data = get_data(day=12, year=2020).split('\n')


# part 1
horizontal = 0
vertical = 0

degree = 0

for inst in data:
    if inst.startswith('N'):
        vertical += int(inst[1:])
    if inst.startswith('E'):
        horizontal += int(inst[1:])
    if inst.startswith('S'):
        vertical -= int(inst[1:])
    if inst.startswith('W'):
        horizontal -= int(inst[1:])

    if inst.startswith('R'):
        degree -= int(inst[1:])

    if inst.startswith('L'):
        degree += int(inst[1:])

    if inst.startswith('F'):
        inst_start = 'ENWS'[(degree // 90) % 4]
        if inst_start == 'N':
            vertical += int(inst[1:])
        if inst_start == 'E':
            horizontal += int(inst[1:])
        if inst_start == 'S':
            vertical -= int(inst[1:])
        if inst_start == 'W':
            horizontal -= int(inst[1:])

print(abs(horizontal) + abs(vertical))

# part 2
horizontal = 0
vertical = 0

degree = 0

waypoint_x = 10
waypoint_y = 1

for inst in data:
    if inst.startswith('N'):
        waypoint_y += int(inst[1:])
    if inst.startswith('E'):
        waypoint_x += int(inst[1:])
    if inst.startswith('S'):
        waypoint_y -= int(inst[1:])
    if inst.startswith('W'):
        waypoint_x -= int(inst[1:])

    if inst.startswith('L'):
        # i forgot all my math so
        # https://www.mashupmath.com/blog/geometry-rotations-90-degrees-clockwise
        amount = int(inst[1:]) % 360
        if amount == 90:
            waypoint_x, waypoint_y = -waypoint_y, waypoint_x
        if amount == 180:
            waypoint_x, waypoint_y = -waypoint_x, -waypoint_y
        if amount == 270:
            waypoint_x, waypoint_y = waypoint_y, -waypoint_x
        if amount == 0:
            waypoint_x, waypoint_y = waypoint_x, waypoint_y

    if inst.startswith('R'):
        # i forgot all my math so
        # https://www.mashupmath.com/blog/geometry-rotations-90-degrees-clockwise
        amount = int(inst[1:]) % 360
        if amount == 90:
            waypoint_x, waypoint_y = waypoint_y, -waypoint_x
        if amount == 180:
            waypoint_x, waypoint_y = -waypoint_x, -waypoint_y
        if amount == 270:
            waypoint_x, waypoint_y = -waypoint_y, waypoint_x
        if amount == 0:
            waypoint_x, waypoint_y = waypoint_x, waypoint_y

    if inst.startswith('F'):
        for i in range(int(inst[1:])):
            horizontal += waypoint_x
            vertical += waypoint_y

print(abs(horizontal) + abs(vertical))
