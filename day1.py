from aocd import get_data

data = get_data(day=1, year=2020)

blah = data.split('\n')

# part 1
for number1 in blah:
    for number2 in blah:
        if int(number1) + int(number2) == 2020:
            print(int(number1) + int(number2))

# part 2
for number1 in blah:
    for number2 in blah:
        for number3 in blah:
            if int(number1) + int(number2) + int(number3) == 2020:
                print(int(number1) * int(number2) * int(number3))
