from aocd import get_data
from collections import defaultdict

data = get_data(day=24, year=2020).split('\n')

blacks = set()

def decode_tile(tile):
    # (full_x, partial_x, partial_y)
    full_x = 0
    partial_x = 0
    partial_y = 0
    char = ''
    while tile:
        char += tile[0]
        tile = tile[1:]

        if char == 'e':
            char = ''
            full_x += 1
        elif char == 'w':
            char = ''
            full_x -= 1
        elif char == 'se':
            char = ''
            partial_x += 1
            partial_y -= 1
        elif char == 'sw':
            char = ''
            partial_x -= 1
            partial_y -= 1
        elif char == 'ne':
            char = ''
            partial_x += 1
            partial_y += 1
        elif char == 'nw':
            partial_x -= 1
            partial_y += 1
            char = ''

    full_x += partial_x * 0.5

    return (full_x, partial_y)


for tile in data:
    res = decode_tile(tile)
    if res in blacks:
        blacks.remove(res)
    else:
        blacks.add(res)

# part 1
print(len(blacks))

def add(a, b):
    return (a[0] + b[0], a[1] + b[1])

def blacks_adjacent(tile):
    amount = 0
    for change in ['e', 'w', 'ne', 'nw', 'se', 'sw']:
        if add(tile, decode_tile(change)) in blacks:
            amount += 1

    return amount

def whites_adjacent(tile):
    whites = []
    for change in ['e', 'w', 'ne', 'nw', 'se', 'sw']:
        if add(tile, decode_tile(change)) not in blacks:
            whites.append(add(tile, decode_tile(change)))

    return whites


i = 0
def day_passes():
    global blacks
    to_switch = set()

    adjacent = defaultdict(lambda: 0)

    for tile in blacks:
        if blacks_adjacent(tile) == 0 or blacks_adjacent(tile) > 2:
            to_switch.add(tile)
        for white in whites_adjacent(tile):
            adjacent[white] += 1

    for thing, amount in adjacent.items():
        if amount == 2:
            to_switch.add(thing)

    for to_switch_tile in to_switch:
        if to_switch_tile in blacks:
            blacks.remove(to_switch_tile)
        else:
            blacks.add(to_switch_tile)

for _ in range(100):
    day_passes()
    i += 1

print(len(blacks))
