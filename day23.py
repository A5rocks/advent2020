from aocd import get_data
from collections import deque


# only part 2 cause... i forgot p1 and it was easy.
data = list(get_data(day=23, year=2020))

# i was so dumb. forgot you were indexing by value and not index.
indexes = {}

class Node:
    val: int
    next_: 'Node'

    def __init__(self, val):
        self.val = val

    def set_behind(self, behind):
        behind.next_ = self

    def __repr__(self):
        return repr(self.val)

# just wanna store smth
cups = []
for thing in data:
    cups.append(Node(int(thing)))
    indexes[int(thing)] = cups[-1]

for i in range(10, 1_000_001):
    cups.append(Node(i))
    indexes[i] = cups[-1]

prev = cups[-1]
for cup in cups:
    cup.set_behind(prev)
    prev = cup

current = cups[0]
for _ in range(10_000_000):
    # ok so, we want to take the next 3
    p1 = current.next_
    p2 = p1.next_
    p3 = p2.next_

    # now then, we do some.... magic
    val = current.val - 1
    while val in (p1.val, p2.val, p3.val) or val < 1:
        if val > 1:
            val -= 1
        else:
            val = 1_000_000

    if val == current.val:
        current = p1
    else:
        # :^)
        temp1 = indexes[val].next_
        previous = current
        current = p3.next_

        # unsafe
        indexes[val].next_ = p1
        p3.next_ = temp1
        previous.next_ = current

while current.val != 1:
    current = current.next_

print(current.next_.val, current.next_.next_.val)
