from aocd import get_data

data = get_data(day=2, year=2020).split('\n')

# part 1
result = 0

for line in data:
    p1, p2, p3 = line.split(' ')
    p11, p12 = map(int, p1.split('-'))

    results = p3.count(p2[:-1])
    if p11 <= results <= p12:
        result += 1

print(result)

# part 2
result = 0

for line in data:
    p1, p2, p3 = line.split(' ')
    p11, p12 = map(int, p1.split('-'))
    
    if p3[p11-1] != p3[p12-1] and p2[:-1] in [p3[p11-1], p3[p12-1]]:
        result += 1

print(result)
