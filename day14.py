from aocd import get_data

data = get_data(day=14, year=2020).split('\n')

# part 1
def apply_mask(mask, on):
    final = list(mask[::-1])
    on = bin(on)[2:][::-1]
    
    for i, char in enumerate(final):
        if char == 'X':
            if i < len(on):
                final[i] = on[i]
            else:
                final[i] = '0'

    return ''.join(final)[::-1]

registers = {}
cur_mask = ''

for thing in data:
    register, thing = thing.split(' = ', 1)
    if register == 'mask':
        cur_mask = thing
    else:
        register = register[4:-1]

        registers[register] = int(apply_mask(cur_mask, int(thing)), 2)

print(sum(registers.values()))

# part 2
def apply_mask(mask, on):
    final = list(mask[::-1])
    on = bin(on)[2:][::-1]
    
    for i, char in enumerate(final):
        if char == '0':
            if i < len(on):
                final[i] = on[i]
            else:
                final[i] = '0'

    return ''.join(final)[::-1]

def apply_floating(result: str) -> 'list[int]':
    if 'X' not in result:
        return [int(result, 2)]

    a = result.replace('X', '1', 1)
    b = result.replace('X', '0', 1)

    return apply_floating(b) + apply_floating(a)

registers = {}
cur_mask = ''

for thing in data:
    register, thing = thing.split(' = ', 1)
    if register == 'mask':
        cur_mask = thing
    else:
        register = register[4:-1]

        x = apply_mask(cur_mask, int(register))

        res = apply_floating(x)
        for i, n in enumerate(res):
            registers[n] = int(thing)

print(sum(registers.values()))