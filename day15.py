from aocd import get_data
from collections import defaultdict

data = map(int, get_data(day=15, year=2020).split(','))
# data = map(int, "0,3,6".split(','))

numbers_ = list(data)

def part_1(n):
    numbers = numbers_.copy()
    for i in range(len(numbers) + 1, n + 1):
        last = numbers[-1]
        indexes = []
        for j, n in enumerate(numbers):
            if n == last:
                indexes.append(j)

        if len(indexes) == 1:
            numbers.append(0)
        else:
            numbers.append(indexes[-1] - indexes[-2])

    return numbers[-1]

def part_2(n):
    last_n = {
        0: (0, 1),
        13: (0, 2),
        1: (0, 3),
        16: (0, 4),
        6: (0, 5)
    }

    last_number = numbers_[-1]

    for i in range(len(numbers_) + 1, n + 1):
        last = last_number

        if i % 1_000_000 == 0:
            print('increment of 1 million')

        if last in last_n:
            p = last_n[last][1]
            last_n[last] = (p, i - 1)
            last_number = i - p - 1
        else:
            last_n[last] = (0, i - 1)
            last_number = 0

    return last_number

print(part_1(2020))
print(part_2(30_000_000))
