from aocd import get_data
import functools

data = list(map(int, get_data(day=10, year=2020).split('\n')))

# part 1
blah = sorted(data)
s1 = 0
s3 = 0
last = 0
for i, thing in enumerate(blah):
    dif = thing - last

    if dif == 1:
        s1 += 1
    elif dif == 3:
        s3 += 1

    last = thing


print(s1 * (s3+1))

# part 2
@functools.lru_cache
def calc(blah):
    res = 1
    if len(blah) == 1:
        # not _really_ a sequence
        return 0

    if blah[1] - blah[0] not in [1, 2, 3]:
        return 0

    for i, thing in enumerate(blah):
        if i == len(blah) - 1:
            return res

        if blah[i+1] - thing in [1, 2]:
            res += calc((thing,) + blah[i+2:])

print(calc(tuple(sorted([0] + data + [max(data) + 3]))))
