from aocd import get_data

program = get_data(day=8, year=2020).split('\n')

# part 1
index = 0
num_inst = 0
acc = 0
instruct_at = [-1 for _ in range(len(program))]

while instruct_at[index] == -1:
    instruct_at[index] = num_inst
    inst, arg = program[index].split(' ', 1)
    arg = int(arg[1:]) if arg.startswith('+') else -int(arg[1:])

    if inst == 'nop':
        index += 1
        num_inst += 1
    elif inst == 'jmp':
        num_inst += 1
        index += arg
    elif inst == 'acc':
        index += 1
        num_inst += 1
        acc += arg

print(acc)

# part 2
def calculate(prgm):
    acc = 0
    index = 0
    num_inst = 0
    instruct_at = [-1 for _ in range(len(prgm))]
    while index < len(prgm) and instruct_at[index] == -1:
        instruct_at[index] = num_inst
        inst, arg = prgm[index].split(' ', 1)
        arg = int(arg[1:]) if arg.startswith('+') else -int(arg[1:])

        if inst == 'nop':
            index += 1
            num_inst += 1
        elif inst == 'jmp':
            num_inst += 1
            index += arg
        elif inst == 'acc':
            index += 1
            num_inst += 1
            acc += arg

    return (index, acc)

for i, instruction in enumerate(program):
    if instruction.startswith('jmp'):
        idx, acc = calculate(program[:i] + [instruction.replace('jmp', 'nop')] + program[i+1:])

        if idx == len(program):
            print(acc)
    elif instruction.startswith('nop'):
        idx, acc = calculate(program[:i] + [instruction.replace('nop', 'jmp')] + program[i+1:])

        if idx == len(program):
            print(acc)
