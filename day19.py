from aocd import get_data
import attr
import typing
import re

sections = get_data(day=19, year=2020).split('\n\n')

# part 2. too lazy to recover part 1
class Rule(typing.Protocol):
    def matches(self, __string: str) -> typing.Tuple[bool, str]:
        ...

    def compile(self) -> str:
        ...

rules: typing.Dict[str, Rule] = {}

@attr.s(auto_attribs=True)
class BaseRule(Rule):
    contents: str

    def matches(self, string):
        return string.startswith(self.contents), string[len(self.contents):]


@attr.s(auto_attribs=True, init=False)
class Union(Rule):
    parts: typing.List[Rule]

    def __init__(self, string):
        self.parts = []
        for part in string.split(' | '):
            self.parts.append(parse(part))

    def matches(self, string):
        for part in self.parts:
            res, rest = part.matches(string)
            if res:
                return True, rest

        return False, string

@attr.s(auto_attribs=True, init=False)
class Combination(Rule):
    parts: typing.List[str]

    def __init__(self, string):
        self.parts = []
        for part in string.split(' '):
            self.parts.append(part)

    def matches(self, string, skip: int = 0):
        n = 0
        for i, part in enumerate(self.parts):
            if n < skip:
                n+=1
                continue

            if part == '8':
                while True:
                    a, string = rules['42'].matches(string)
                    if not a:
                        return False, string
                    
                    stop, rest = self.matches(string, skip=i+1)
                    if stop:
                        return True, rest
            elif part == '11':
                amount = 1
                continue_rounds = True

                while continue_rounds:
                    string_ = string
                    for _ in range(amount):
                        a, string_ = rules['42'].matches(string_)

                        if not a:
                            return False, string_

                    for _ in range(amount):
                        a, string_ = rules['31'].matches(string_)

                        if not a: break
                    if not a and len(string):
                        amount += 1
                        continue

                    stop, rest = self.matches(string_, skip=i+1)
                    if stop:
                        return True, rest

                    amount += 1
            else:
                res, string = rules[part].matches(string)

                if not res:
                    return False, string

        return True, string


def parse(rule: str, n=0) -> Rule:
    if n == 8:
        return Rule8()
    elif n == 11:
        return Rule11()
    elif '"' in rule:
        return BaseRule(eval(rule))
    elif '|' in rule:
        return Union(rule)
    else:
        return Combination(rule)

rules_, transmissions = sections
for rule in rules_.split('\n')[::-1].copy():
    n, rule = rule.split(': ', 1)

    if n == '8':
       rule = '42 | 42 8'
    if n == '11':
        rule = '42 31 | 42 11 31'

    rules[n] = parse(rule, n=n)

result = 0

for message in transmissions.split('\n'):
    n, thing = rules['0'].matches(message)
    if n and thing == '':
        result += 1

print(result)
