from aocd import get_data

data = get_data(day=11, year=2020).split('\n')

# part 1
# lost to the sands of time, don't wanna redo it

# part 2
def visible_from(x, y, seats):
    res = 0

    # horizontal
    chairs_left, chairs_right = seats[y][:x], seats[y][x+1:]
    res += sum(1 for seq in (chairs_left[::-1], chairs_right) if _visible_char(seq))

    # vertical
    chairs_up, chairs_down = [seats[i][x] for i in range(y)], [seats[i][x] for i in range(y + 1, len(seats))]
    res += sum(1 for seq in (chairs_up[::-1], chairs_down) if _visible_char(seq))

    # diagonals
    up_left = [seats[y - n][x - n] for n in range(1, min(x, y) + 1)]
    up_right = [seats[y - n][x + n] for n in range(1, min(len(seats[0]) - x - 1, y) + 1)]
    down_left = [seats[y + n][x - n] for n in range(1, min(x, len(seats) - y - 1) + 1)]
    down_right = [seats[y + n][x + n] for n in range(1, min(len(seats[0]) - x - 1, len(seats) - y - 1) + 1)]
    res += sum(1 for seq in (up_left, up_right, down_left, down_right) if _visible_char(seq))

    return res

def _visible_char(sequence):
    for elem in sequence:
        if elem == 'L':
            return False
        elif elem == '#':
            return True

    return False



def simulate(seats):
    out = []

    for i, row in enumerate(seats):
        output = ''
        if i > 0:
            prev_seats = seats[i - 1]
        else:
            prev_seats = '.' * len(row)

        if i < len(seats) - 1:
            next_seats = seats[i + 1]
        else:
            next_seats = '.' * len(row)

        for j, chair in enumerate(row):
            visible = visible_from(j, i, seats)

            if chair == '.':
                output += '.'
            elif chair == 'L':
                if visible == 0:
                    output += '#'
                else:
                    output += 'L'
            else:
                if visible >= 5:
                    output += 'L'
                else:
                    output += '#'

        out.append(output)

    return out

last = None
while result != last:
    last = result
    result = simulate(result)


print(sum(sum(1 for seat in row if seat == '#') for row in result))