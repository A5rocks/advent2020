from aocd import get_data

data = get_data(day=3, year=2020).split('\n')

curr_x = 0
curr_y = 0
result = 0

# part 1
while curr_y < len(data):
    if data[curr_y][curr_x % len(data[curr_y])] == '#':
        result += 1

    curr_y += 1
    curr_x += 3

print(result)

# part 2
def f(dx, dy):
    curr_x = 0
    curr_y = 0
    result = 0

    while curr_y < len(data):
        if data[curr_y][curr_x % len(data[curr_y])] == '#':
            result += 1

        curr_y += dy
        curr_x += dx

    return result

print(f(1, 1) * f(3, 1) * f(5, 1) * f(7, 1) * f(1, 2))
