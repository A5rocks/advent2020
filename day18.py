from aocd import get_data

data = get_data(day=18, year=2020).split('\n')

# part 1
def get_paren(line):
    paren_count = int(line.endswith(')'))
    output = ')' if line.endswith(')') else ''
    rest = line[:-1] if line.endswith(')') else line

    while paren_count:
        start = rest[-1]
        rest = rest[:-1]

        if start == '(':
            paren_count -= 1
        elif start == ')':
            paren_count += 1

        output = start + output

    return output, rest


def evaluate(line) -> int:
    # break up to the first operator
    line = line.strip()

    if line.split(' ')[0].endswith(')'):
        start, rest = get_paren(' '.join(line.split(' ')[::-1]))
        rest = ' '.join(rest.strip().split(' ')[::-1])
        start = ' '.join(start.strip()[1:-1].split(' ')[::-1])

        if rest.startswith('+'):
            return evaluate(start) + evaluate(rest[1:])
        elif rest.startswith('*'):
            return evaluate(start) * evaluate(rest[1:])
        else:
            return evaluate(start)
    elif '+' in line and ('*' not in line or line.index('+') < line.index('*')):
        start, rest = line.split('+', 1)
        return int(start) + evaluate(rest)
    elif '*' in line:
        start, rest = line.split('*', 1)
        return int(start) * evaluate(rest)
    else:
        return int(line)

result = 0
for line in data:
    result += evaluate(" ".join(line.split(" ")[::-1]))

print(result)

# part 2
from aocd import get_data

data = get_data(day=18, year=2020).split('\n')

def get_paren(line):
    paren_count = int(line.endswith(')'))
    output = ')' if line.endswith(')') else ''
    rest = line[:-1] if line.endswith(')') else line

    while paren_count:
        start = rest[-1]
        rest = rest[:-1]

        if start == '(':
            paren_count -= 1
        elif start == ')':
            paren_count += 1

        output = start + output

    return output, rest


def evaluate(line) -> int:
    # break up to the first operator
    line = line.replace('+', ' + ')
    line = line.replace('*', ' * ')
    line = ' '.join(piece for piece in line.split(' ') if piece != '')
    line = line.strip()
    if ')' in line:
        first_index = 0
        for thing in line.split(' '):
            if ')' in thing:
                break
            first_index += 1

        cur_line, line = ' '.join(line.split(' ')[:first_index]), ' '.join(line.split(' ')[first_index:])
        start, rest = get_paren(' '.join(line.split(' ')[::-1]))
        rest = ' '.join(rest.strip().split(' ')[::-1])
        start = ' '.join(start.strip()[1:-1].split(' ')[::-1])

        return evaluate(cur_line + str(evaluate(start)) + rest)
    elif '+' in line:
        a, b = 0, 0
        before, after = '', ''

        for i, thing in enumerate(line.split(' ')):
            if thing == '+':
                a, b = line.split(' ')[i-1], line.split(' ')[i+1]
                before, after = ' '.join(line.split(' ')[:i-1]), ' '.join(line.split(' ')[i+2:])
                break
        else:
            print(line.split(' '))
            raise Exception()

        return evaluate(before + str(int(a) + int(b)) + after)
    elif '*' in line:
        start, rest = line.split('*', 1)
        return int(start) * evaluate(rest)
    else:
        return int(line)

result = 0
for line in data:
    result += evaluate(" ".join(line.split(" ")[::-1]))

print(result)
