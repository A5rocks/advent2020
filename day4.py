from aocd import get_data
import attr

data = get_data(day=4, year=2020).split('\n')

# split into passports
passports = []
last_line = ''
result = 0

for line in data:
    if line == '':
        passports.append(last_line)
        last_line = ''

    last_line += line
    last_line += ' '

# i messed this up for my actual program and was wondering why I was off by one
passports.append(last_line)

for passport in passports:
    stuffs = passport.split(' ')
    keys = set()
    for kv in stuffs:
        if not kv: continue
        k, v = kv.split(':', 1)
        keys |= {k}

    if keys >= {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'}:
        result += 1

print(result)
result = 0

for passport in passports:
    stuffs = passport.split(' ')
    keys = set()
    for kv in stuffs:
        if not kv: continue
        k, v = kv.split(':', 1)
        

        try:
            if k == 'byr':
                assert 1920 <= int(v) <= 2002
            elif k == 'iyr':
                assert 2010 <= int(v) <= 2020
            elif k == 'eyr':
                assert 2020 <= int(v) <= 2030
            elif k == 'hgt':
                if v.endswith('cm'):
                    assert 150 <= int(v[:-2]) <= 193
                elif v.endswith('in'):
                    assert 59 <= int(v[:-2]) <= 72
                else:
                    assert False
            elif k == 'hcl':
                assert v.startswith('#')
                assert all(char in list('0123456789abcdef') for char in v[1:])
            elif k == 'ecl':
                assert v in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']
            elif k == 'pid':
                assert len(v)
                int(v)
        except:
            continue

        keys |= {k}

    if keys >= {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'}:
        result += 1

print(result)

