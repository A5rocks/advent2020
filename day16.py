from aocd import get_data

stuff, my_ticket, others = get_data(day=16, year=2020).split('\n\n')

# part 1
rules = []

for rule in stuff.split('\n'):
    a, b = rule.split(': ')[1].split(' or ')
    c, d = a.split('-')
    e, f = b.split('-')

    rules.append(((int(c), int(d)), (int(e), int(f))))

result = 0

for ticket in others.split('\n')[1:]:
    for field in ticket.split(','):
        if all(not (a[0] <= int(field) <= a[1] or b[0] <= int(field) <= b[1]) for a, b in rules):
            result += int(field)

print(result)


# part 2
rules = {}
rule_names = []

for rule in stuff.split('\n'):
    name, x = rule.split(': ')
    a, b = x.split(' or ')
    c, d = a.split('-')
    e, f = b.split('-')

    rules[name] = ((int(c), int(d)), (int(e), int(f)))
    rule_names.append(name)

result = 0
valid = []
my_ticket = my_ticket.split('\n')[1]

def satisfies(field, rule):
    field = int(field)
    a, b = rule
    return a[0] <= int(field) <= a[1] or b[0] <= int(field) <= b[1]

for ticket in others.split('\n')[1:]:
    for field in ticket.split(','):
        if all(not satisfies(field, rule) for rule in rules.values()):
            break
    else:
        valid.append(ticket)

possibilities = {k: rule_names.copy() for k in range(len(my_ticket.split(',')))}

for ticket in valid:
    for i, field in enumerate(ticket.split(',')):
        for possibility_ in possibilities[i]:
            possibility = rules[possibility_]
            if not satisfies(field, possibility):
                possibilities[i].remove(possibility_)
                break

# :)
for x in range(100):
    for j, possibility in possibilities.items():
        if len(possibility) == 1:
            for i in possibilities.keys():
                if i != j:
                    if possibility[0] in possibilities[i]:
                        possibilities[i].remove(possibility[0])

result = 1

for key, value in possibilities.items():
    if value[0].startswith('departure'):
        print('count')
        result *= int(my_ticket.split(',')[key])

print(result)
